package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

func fetchAndWriteData(endpoint string, wg *sync.WaitGroup, startTime time.Time) {
	defer wg.Done()

	apiUrl := fmt.Sprintf("https://jsonplaceholder.typicode.com/todos/%s", endpoint)

	response, err := http.Get(apiUrl)
	if err != nil {
		fmt.Printf("Error al realizar la solicitud HTTP para %s: %v\n", endpoint, err)
		return
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Printf("Error al leer la respuesta para %s: %v\n", endpoint, err)
		return
	}

	filename := fmt.Sprintf("data_%s.json", endpoint)
	err = ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		fmt.Printf("Error al escribir en el archivo para %s: %v\n", endpoint, err)
		return
	}

	elapsed := time.Since(startTime)
	fmt.Printf("Datos descargados y escritos en '%s' (Tiempo: %s)\n", filename, elapsed)
}

func main() {
	var wg sync.WaitGroup
	startTime := time.Now()

	for i := 1; i <= 200; i++ {
		endpoint := fmt.Sprintf("%d", i)
		wg.Add(1)
		go fetchAndWriteData(endpoint, &wg, startTime)
	}

	wg.Wait()

	totalTime := time.Since(startTime)
	fmt.Printf("Tiempo total de ejecución: %s\n", totalTime)
}
